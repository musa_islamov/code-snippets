#Примеры кода
- deploy
    - ansible (пример настройки nginx proxy с помощью Ansible)
    - terraform (пример разворачивания инфраструктуры Yandex Cloud [`очередь`, `контейнер`, `serverless-база данных`, `api-gateway`])
    - docker
        - Dockerfile для Python и compose-файл
    - gitlab-ci файл 
    
- project_1
    - `emails:` примеры celery-задач для отправки писем
    
- project_2
    - `drf_views:` примеры view для DjangoRestFramework
    - `models`: примеры Django-моделей
    - `payments`: пример интеграции с платежным сервисом Stripe (stripe.com)
    - `tasks`: пример celery-задач для расчета кэшбека, писем и ресайза картинок с конвертацией в WEBP и загрузки в S3
    - `settings:` пример настроек для celery с использованием AWS sqs-queue
  
В некоторых местах sensetive-data показывается как `***`