def _render_to_string_with_email_additional_info(template, context: dict):
    context.update(
        {
            "HELP_PORTAL_LINK": settings.HELP_PORTAL_LINK,
            "SUPPORT_EMAIL": settings.SUPPORT_EMAIL,
            "SUPPORT_PHONE": settings.SUPPORT_PHONE,
        }
    )
    return render_to_string(template, context)


def _get_user_avatar_url(user: User):
    if user.avatar:
        avatar_url = user.avatar.thumbnail.url
    else:
        filename, avatar = generate_avatar(user.name)
        avatar_base64 = base64.b64encode(avatar.read()).decode()
        avatar_url = f"data:image/png;base64,{avatar_base64}"
    return avatar_url


@app.task(queue="emails")
def send_mail_about_created_project_to_admins(project_id: int, user_id: int, site_address: str, admin_link: str):
    try:
        emails = [e for e in settings.ADMIN_MAIL_LIST if len(e) > 0]
        if len(emails) == 0:
            return
        project = Project.objects.get(pk=project_id)
        user = User.objects.get(pk=user_id)
        send_mail(
            _("%(project)s — %(user)s — %(site_address)s")
            % {"project": project.name, "user": user.email, "site_address": site_address},
            _(
                "Site address: %(site_address)s\n"
                "Date: %(date)s\n"
                "Username: %(username)s\n"
                "Company name: %(company)s\n"
                "Project name: %(project)s\n"
                "Estimate link: %(estimate)s\n"
                "Project page in the admin panel: %(admin_link)s"
            )
            % {
                "site_address": site_address,
                "date": date_format(date.today(), "DATE_FORMAT"),
                "username": user.name,
                "company": project.settings.user_company_name,
                "project": project.name,
                "estimate": project.file.url if project.file else "No file",
                "admin_link": admin_link,
            },
            emails,
        )
    except Exception as e:
        capture_exception(e)


@app.task(queue="emails")
def send_mail_about_registration(
    user_id: int, email_confirmation_hash: str, client_name: Optional[str] = "", only_render=False
):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    html = _render_to_string_with_email_additional_info(
        "mailing/verify-email.html",
        {"confirmation_code": email_confirmation_hash, "client_name": client_name if client_name else user.email},
    )
    if only_render:
        return html
    send_html_email(html, subject=_("Welcome to Fixtender! Please verify your email"), recipients=[user.email])


@app.task(queue="emails")
def send_mail_about_email_change(user_id: int, user_name: str, email_confirmation_hash: str, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    html = _render_to_string_with_email_additional_info(
        "mailing/change-email.html",
        {"link": settings.CONFIRM_EMAIL_LINK.format(email_confirmation_hash), "user_name": user_name},
    )
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(user_name)s, your email has been changed in the account settings") % {"user_name": user_name},
        recipients=[user.email],
    )


@app.task(queue="emails")
def send_mail_about_password_recovery(
    user_id: int, user_name: str, password_recovery_confirmation_hash: str, only_render=False
):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    html = _render_to_string_with_email_additional_info(
        "mailing/password-recovery.html",
        {"user_name": user_name, "link": settings.RECOVERY_LINK.format(password_recovery_confirmation_hash)},
    )
    if only_render:
        return html
    send_html_email(html, subject=_("Restore password"), recipients=[user.email])


@app.task(queue="emails")
def send_mail_about_password_change(user_id: int, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    html = _render_to_string_with_email_additional_info("mailing/password-changed.html", {})
    if only_render:
        return html
    send_html_email(html, subject=_("Changing password"), recipients=[user.email])


@app.task(queue="emails")
def send_invitation_to_team_for_new_user(
    user_id: int, user_name: str, one_who_invites: str, invitation_hash: str, team_name: str, only_render=False
):
    """Should only be sent to new users in any role"""
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    html = _render_to_string_with_email_additional_info(
        "mailing/invite-new-user-to-team.html",
        {
            "user_name": user_name,
            "inviter_name": one_who_invites,
            "team": team_name,
            "link": settings.INVITATION_LINK.format(invitation_hash),
        },
    )
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(name)s invites you to work on Fixtender for %(team)s")
        % {"name": one_who_invites, "team": team_name},
        recipients=[user.email],
        from_name=_("%(team_name)s  via Fixtender") % {"team_name": team_name},
    )


@app.task(queue="emails")
def send_invitation_to_team_for_registered_user(
    user_id: int, name_of_user_who_invites: str, team_name: str, only_render=False
):
    """Should be sent only to registered user which is added to team as administrator"""
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    html = _render_to_string_with_email_additional_info(
        "mailing/invite-activated-user-to-new-team.html",
        {"name": name_of_user_who_invites, "team": team_name, "link": settings.PROJECTS_PAGE_LINK},
    )
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(name)s invites you to join '%(team)s' on Fixtender")
        % {"name": name_of_user_who_invites, "team": team_name},
        recipients=[user.email],
    )


@app.task(queue="emails")
def send_contractor_invite_to_tender(user_id, tender_offer_id, inviter_company_name, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    tender_offer = TenderOffer.objects.get(id=tender_offer_id)
    responsible_user_name = tender_offer.tender.contact_person_responsible_user.name
    responsible_user_phone = str(tender_offer.tender.contact_person_responsible_user.phone_number)
    avatar_url = _get_user_avatar_url(tender_offer.tender.contact_person_responsible_user)

    link = settings.PUBLIC_TENDER_LINK_WITH_OFFER.format(
        tender_offer.tender.public_link_hash, tender_offer.public_link_hash
    )

    context = {
        "user_name": user.name,
        "user_email": user.email,
        "tender_offer": tender_offer,
        "project_name": tender_offer.tender.project.name,
        "tender_name": tender_offer.tender.name,
        "responsible_user_name": responsible_user_name,
        "responsible_user_phone": responsible_user_phone,
        "inviter_company_name": inviter_company_name,
        "avatar_url": avatar_url,
        "link": link,
    }
    html = _render_to_string_with_email_additional_info("mailing/contractor-invite-to-tender.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("👷New tender from %(inviter_company_name)s") % context,
        recipients=[user.email],
        from_name=_("%(responsible_user_name)s from %(inviter_company_name)s") % context,
    )


@app.task(queue="emails")
def send_offer_imported_and_submitted(user_id, tender_offer_id, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    tender_offer = TenderOffer.objects.get(id=tender_offer_id)
    responsible_user_name = tender_offer.tender.contact_person_responsible_user.name
    avatar_url = _get_user_avatar_url(tender_offer.tender.contact_person_responsible_user)

    context = {
        "user_name": user.name,
        "user_email": user.email,
        "responsible_user_name": responsible_user_name,
        "tender_offer": tender_offer,
        "tender_name": tender_offer.tender.name,
        "general_contractor_company_name": tender_offer.tender.project.team.name,
        "inviter_company_name": tender_offer.tender.project.team.name,
        "avatar_url": avatar_url,
    }
    html = _render_to_string_with_email_additional_info("mailing/offer-imported-and-submitted.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("👷%(tender_name)s – We’ve received your offer") % context,
        recipients=[user.email],
        from_name=_("%(responsible_user_name)s from %(general_contractor_company_name)s") % context,
    )


@app.task(queue="emails")
def send_offer_imported(user_id, tender_offer_id, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    tender_offer = TenderOffer.objects.get(id=tender_offer_id)

    context = {
        "user_name": user.name,
        "user_email": user.email,
        "tender_offer": tender_offer,
        "tender_name": tender_offer.tender.name,
        "link": settings.INCOMING_TENDER_OFFER_PAGE_LINK.format(tender_offer.id),
    }
    html = _render_to_string_with_email_additional_info("mailing/offer-imported.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("👷%(tender_name)s – We converted your Excel file") % context,
        recipients=[user.email],
        from_name=_("Fixtender platform"),
    )


@app.task(queue="emails")
def send_offer_import_errors(user_id, tender_offer_id, problems_text, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    tender_offer = TenderOffer.objects.get(id=tender_offer_id)

    context = {
        "user_name": user.name,
        "user_email": user.email,
        "tender_offer": tender_offer,
        "tender_name": tender_offer.tender.name,
        "general_contractor_company_name": tender_offer.tender.project.team.name,
        "inviter_company_name": tender_offer.tender.project.team.name,
        "problems_text": problems_text,
    }
    html = _render_to_string_with_email_additional_info("mailing/offer-import-errors.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("👷%(tender_name)s – We found errors in your Excel file") % context,
        recipients=[user.email],
        from_name=_("Fixtender platform"),
    )


@app.task(queue="emails")
def send_offer_version_submit(user_id, user_name, tender_name, company_name, link, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    context = {"tender_name": tender_name, "user_name": user_name, "company_name": company_name, "link": link}
    html = _render_to_string_with_email_additional_info("mailing/offer-version-submit.html", context)
    if only_render:
        return html
    send_html_email(
        html, subject=_("New offer for %(tender_name)s from %(company_name)s") % context, recipients=[user.email]
    )


@app.task(queue="emails")
def send_tender_offer_version_winner(user_id, tender_name, inviter_name, link, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    context = {"tender_name": tender_name, "inviter_name": inviter_name, "link": link}
    html = _render_to_string_with_email_additional_info("mailing/tender-offer-version-winner.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(inviter_name)s: has chosen you as the winner for %(tender_name)s") % context,
        recipients=[user.email],
    )


@app.task(queue="emails")
def send_tender_offer_version_loser(user_id, user_name, tender_name, inviter_name, link, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    context = {"tender_name": tender_name, "user_name": user_name, "inviter_name": inviter_name, "link": link}
    html = _render_to_string_with_email_additional_info("mailing/tender-offer-version-loser.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(tender_name)s: %(inviter_name)s has chosen another company to work with" % context),
        recipients=[user.email],
    )


@app.task(queue="emails")
def send_tender_info_change(user_id, tender_name, inviter_name, link, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    context = {"tender_name": tender_name, "inviter_name": inviter_name, "link": link}
    html = _render_to_string_with_email_additional_info("mailing/tender-info-change.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(tender_name)s: %(inviter_name)s added more information to the tender") % context,
        recipients=[user.email],
    )


@app.task(queue="emails")
def send_tender_conditions_change(
    tender_offer_id, user_id, user_name, tender_name, inviter_name, link, only_render=False
):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    tender_offer = TenderOffer.objects.get(pk=tender_offer_id)
    avatar_url = _get_user_avatar_url(tender_offer.tender.contact_person_responsible_user)
    context = {
        "user_name": user_name,
        "tender_offer": tender_offer,
        "tender_name": tender_name,
        "inviter_name": inviter_name,
        "link": link,
        "avatar_url": avatar_url,
    }
    html = _render_to_string_with_email_additional_info("mailing/tender-conditions-change.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(tender_name)s: %(inviter_name)s edited the terms of the tender") % context,
        recipients=[user.email],
    )


@app.task(queue="emails")
def send_company_change_settings_email(user_emails, company_name, only_render=False):
    # TODO localize
    context = {"company_name": company_name}
    html = _render_to_string_with_email_additional_info("mailing/company-change-settings.html", context)
    if only_render:
        return html
    send_html_email(html, subject=_("Company settings have been changed"), recipients=user_emails)


@app.task(queue="emails")
def send_tender_deadline_for_contractor_email(user_id, tender_offer_id, link, only_render=False):
    user = User.objects.get(id=user_id)
    translation.activate(user.language)
    tender_offer = TenderOffer.objects.get(id=tender_offer_id)
    responsible_user_name = tender_offer.tender.contact_person_responsible_user.name
    inviter_company_name = tender_offer.tender.project.team.name
    context = {
        "tender_name": tender_offer.tender.name,
        "link": link,
        "responsible_user_name": responsible_user_name,
        "inviter_company_name": inviter_company_name,
    }
    html = _render_to_string_with_email_additional_info("mailing/tender-info-deadline.html", context)
    if only_render:
        return html
    send_html_email(
        html,
        subject=_("%(tender_name)s: Deadline is coming in 5 days") % context,
        from_name=_("%(responsible_user_name)s from %(inviter_company_name)s") % context,
        recipients=[user.email],
    )
