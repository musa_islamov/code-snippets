terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/38974651/terraform/state/old-state-name"
    lock_address = "https://gitlab.com/api/v4/projects/38974651/terraform/state/old-state-name/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/38974651/terraform/state/old-state-name/lock"
  }
}


variable "yc_token" {
  type     = string
  nullable = false
}

variable "service_account_id" {
  type     = string
  nullable = false
}

variable "yandex_registry" {
  type     = string
  nullable = false
}

variable "cloud_id" {
  type     = string
  nullable = false
}

variable "folder_id" {
  type     = string
  nullable = false
}

variable "access_key" {
  type     = string
  nullable = false
}

variable "secret_key" {
  type     = string
  nullable = false
}

variable "region" {
  type = string
  default = "ru-central1"
}

variable "zone" {
  type     = string
  default = "ru-central1-a"
}

variable "project_name" {
  type = string
  default = "zara-parser"
}

variable "sentry_dsn" {
  type = string
}

variable "image_tag" {
  type = string
  default = "latest"
}

variable "container_path_for_trigger" {
  type = string
  default = "/parser/worker/"
}

variable "postgres_database_name" {
    type = string
}
variable "postgres_database_host" {
    type = string
}
variable "postgres_database_user" {
    type = string
}
variable "postgres_database_password" {
    type = string
}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token = var.yc_token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

resource "yandex_message_queue" "queue" {
  name                        = var.project_name
  visibility_timeout_seconds  = 600
  receive_wait_time_seconds   = 20
  message_retention_seconds   = 1209600
  region_id = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "yandex_ydb_database_serverless" "db" {
  name      = var.project_name
  folder_id = var.folder_id
}


resource "yandex_serverless_container" "container" {
  name               = var.project_name
  description        = "zara parser"
  memory             = 512
  execution_timeout  = "600s"
  cores = 1
  core_fraction = 5
  service_account_id = var.service_account_id
  concurrency = 1
  image {
    url = "${var.yandex_registry}/${var.project_name}"
    digest = var.image_tag
    environment = {
      CELERY_BROKER_URL = "sqs://${var.access_key}:${var.secret_key}@message-queue.api.cloud.yandex.net"
      SQS_QUEUE = yandex_message_queue.queue.id
      SQS_ACCESS_KEY_ID = yandex_message_queue.queue.access_key
      SQS_SECRET_ACCESS_KEY = yandex_message_queue.queue.secret_key
      SQS_REGION = yandex_message_queue.queue.region_id
      CELERY_DYNAMODB_ENDPOINT_URL = yandex_ydb_database_serverless.db.document_api_endpoint
      SENTRY_DSN = var.sentry_dsn
      YANDEX_OAUTH_TOKEN = var.yc_token
      POSTGRES_DATABASE_NAME = var.postgres_database_name
      POSTGRES_USER=var.postgres_database_user
      POSTGRES_PASSWORD=var.postgres_database_password
      POSTGRES_HOST=var.postgres_database_host
      FOLDER_ID=var.folder_id
    }
  }
}

resource "yandex_api_gateway" "api-gateway" {
  name = var.project_name
  description = "zara parser"
  spec = <<-EOT
openapi: "3.0.0"
info:
  version: 1.0.0
  title: Zara parser
paths:
  /{url+}:
    x-yc-apigateway-any-method:
      summary: Execute container
      operationId: container
      parameters:
      - explode: false
        in: path
        name: url
        required: false
        style: simple
      x-yc-apigateway-integration:
        type: serverless_containers
        container_id: ${yandex_serverless_container.container.id}
        service_account_id: ${var.service_account_id}
EOT
}

output "domain" {
  value = "${yandex_api_gateway.api-gateway.domain}"
}
