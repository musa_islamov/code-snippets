def get_product_info(
    product_sku_features,
    image: Union[InfluencerShopItemImage, ProductImage],
    title: str,
    price: int,
    new_price: int = None,
    link: Optional = None,
):
    features = {}
    for feature in product_sku_features:
        features[feature["feature__name"]] = (
            feature["number_value"]
            if feature["number_value"] is not None
            else feature["string_value"]
            if feature["string_value"] is not None
            else feature["boolean_value"]
        )
    if new_price:
        new_price = new_price / 100
        new_price_int_part = str(new_price).split(".")[0]
        new_price_decimal_part = str(new_price).split(".")[1]
    else:
        new_price_int_part = None
        new_price_decimal_part = None
    good_info = {
        "image_url": image,
        "title": title,
        "features": features,
        "price": price / 100,
        "new_price": new_price,
        "price_int_part": str(price / 100).split(".")[0],
        "price_decimal_part": str(price / 100).split(".")[1],
        "new_price_int_part": new_price_int_part,
        "new_price_decimal_part": new_price_decimal_part,
        "link": link,
    }
    return good_info


@app.task(queue="emails")
def send_influencer_account_verified_email(user_id: int, only_render=False):
    user = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    influencers_shop = influencer_shop.InfluencerShop.not_deleted.filter(
        influencer=user
    ).first()
    influencer_shop_name = influencers_shop.name
    external_id = influencers_shop.external_id
    influencer_info = user.influencer_info
    if influencer_info.avatar:
        avatar_url = influencer_info.avatar.url
    else:
        avatar_url = None
    html = _render(
        "emails/redesigned_emails/account-verified.html",
        {
            "avatar_url": avatar_url,
            "influencer_title": influencer_shop_name,
            "link": settings.INFLUENCER_SHOP_LINK.format(external_id),
            "user": user,
        },
    )
    if only_render:
        return html
    send_html_email(_("Account is verified"), [user.email], html)


@app.task(queue="emails")
def send_influencer_problems_with_verification_email(
    user_id: int, problem_text: str, only_render=False
):
    user = User.objects.get(id=user_id)
    translation.activate(settings.LANGUAGE_CODE)
    html = _render(
        "emails/redesigned_emails/problems-with-verification.html",
        {
            "user": user,
            "problem_text": problem_text,
            "link": settings.INFLUENCER_SEND_ACCOUNT_TO_VERIFICATION_LINK,
        },
    )
    if only_render:
        return html
    send_html_email(_("Account is not verified"), [user.email], html)


@app.task(queue="periodic")
def send_buyer_in_stock_again_email(only_render=False):
    today = datetime.datetime.utcnow().date()
    yesterday = datetime.datetime.utcnow().date() - datetime.timedelta(days=1)
    users = User.objects.filter(is_merchant=False, is_influencer=False)
    for user in users:
        translation.activate(settings.LANGUAGE_CODE)
    for user in users:
        goods_info = []
        product_skus = list(
            CartItem.objects.filter(
                Q(user_id=user.pk),
                Q(product_sku__in_stock__gt=0),
                Q(product_sku__in_stock_previously=0),
                Q(is_selected_to_buy=False),
                Q(product_sku__stock_updated_date__date=yesterday)
                | Q(product_sku__stock_updated_date__date=today),
            )
            .prefetch_related("product_sku__product")
            .values_list("product_sku", flat=True)
        ) + list(
            WishListItem.objects.filter(
                Q(user_id=user.pk),
                Q(product_sku__in_stock__gt=0),
                Q(product_sku__in_stock_previously=0),
                Q(product_sku__stock_updated_date__date=yesterday)
                | Q(product_sku__stock_updated_date__date=today),
            )
            .prefetch_related("product_sku__product")
            .values_list("product_sku", flat=True)
        )
        for product_sku in product_skus:
            influencer_shop_item = InfluencerShopItem.objects.prefetch_related(
                "images"
            ).get(product_id=product_sku.product)
            images_exist = influencer_shop_item.images.exists()
            if images_exist:
                image = influencer_shop_item.images.first().image.url
            else:
                image = None
            product = product_sku.product.pk
            product_sku_features = ProductFeatureValue.objects.filter(
                product_id=product.pk, feature__is_deleted=False
            ).values("number_value", "string_value", "boolean_value", "feature__name")
            good_info = get_product_info(
                product_sku_features,
                image,
                influencer_shop_item.name,
                product_sku.price,
                settings.ITEM_PAGE_LINK.format(
                    influencer_shop_item.influencer_shop.external_id,
                    influencer_shop_item.collection.pk,
                    influencer_shop_item.influencer_shop.pk,
                ),
            )
            goods_info.append(good_info)
        html = _render(
            "emails/redesigned_emails/in-stock-again.html",
            {
                "user": user,
                "goods_info": goods_info,
            },
        )
        if only_render:
            return html
        send_html_email(
            _("Several items are in stock again"),
            [user.email],
            html,
        )


@app.task(queue="periodic")
def send_abandoned_cart_email(only_render=False):
    users = User.objects.filter(is_merchant=False)
    for user in users:
        translation.activate(settings.LANGUAGE_CODE)
        goods_info = []
        cart_items = cart_item.CartItem.objects.filter(
            user_id=user.pk, is_selected_to_buy=False
        ).prefetch_related("product_sku__product")
        influencer_cart_items = (
            api.models.order.influencer_cart_item.InfluencerCartItem.objects.filter(
                user_id=user.pk, is_selected_to_buy=False
            )
        )
        if len(cart_items) != 0:
            for item in cart_items:
                influencer_shop_item = (
                    InfluencerShopItem.objects.prefetch_related("images")
                    .filter(product=item.product_sku.product)
                    .first()
                )
                images_exist = influencer_shop_item.images.exists()
                if images_exist:
                    image = influencer_shop_item.images.first().image.url
                else:
                    image = None
                product_sku_features = ProductFeatureValue.objects.filter(
                    product=item.product_sku.product, feature__is_deleted=False
                ).values(
                    "number_value", "string_value", "boolean_value", "feature__name"
                )
                if item.product_sku.compare_at_price:
                    new_product_price = item.product_sku.price
                    old_price = item.product_sku.compare_at_price
                else:
                    new_product_price = None
                    old_price = item.product_sku.price
                good_info = get_product_info(
                    product_sku_features,
                    image,
                    influencer_shop_item.name,
                    old_price,
                    new_product_price,
                    settings.ITEM_PAGE_LINK.format(
                        influencer_shop_item.influencer_shop.external_id,
                        influencer_shop_item.collection.pk,
                        influencer_shop_item.influencer_shop.pk,
                    ),
                )
                goods_info.append(good_info)
        if len(influencer_cart_items) != 0:
            for item in influencer_cart_items:
                product = (
                    api.models.product.Product.objects.prefetch_related("images")
                    .filter(product=item.product_sku.product)
                    .first()
                )
                images_exist = product.images.exists()
                if images_exist:
                    image = product.images.first().image.url
                else:
                    image = None
                product_sku_features = ProductFeatureValue.objects.filter(
                    product=item.product_sku.product, feature__is_deleted=False
                ).values(
                    "number_value", "string_value", "boolean_value", "feature__name"
                )
                if item.product_sku.compare_at_price:
                    new_product_price = item.product_sku.price
                    old_price = item.product_sku.compare_at_price
                else:
                    new_product_price = None
                    old_price = item.product_sku.price
                influencer_good_info = get_product_info(
                    product_sku_features,
                    image,
                    product.name,
                    old_price,
                    new_product_price,
                )
                goods_info.append(influencer_good_info)
        if len(goods_info) != 0:
            html = _render(
                "emails/redesigned_emails/abandoned-cart.html",
                {
                    "user": user,
                    "goods_info": goods_info,
                    "link": settings.BUYER_CART_LINK,
                },
            )

            if only_render:
                return html
            send_html_email(
                _("Your cart is wondering where you went..."), [user.email], html
            )


@app.task(queue="emails")
def send_registered_email(user_id: int, only_render=False):
    user = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    html = _render("emails/redesigned_emails/registered.html", {"user": user})

    if only_render:
        return html
    send_html_email(
        _("You have successfully registered on Shoplio"), [user.email], html
    )


@app.task(queue="emails")
def send_successful_order_email(user_id: int, order_id: int, only_render=False):
    user = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    sleep(3)
    order = api.models.BuyerOrder.objects.filter(pk=order_id).first()
    order_merchant = MerchantOrder.objects.filter(
        buyer_order=order, status=MerchantOrderStatus.new
    ).first()
    goods_info = []
    items_in_order = ProductSKUMerchantOrder.objects.filter(
        merchant_order_id=order_merchant.pk
    ).prefetch_related("influencer_shop_item__images", "product_sku__product")
    for item in items_in_order:
        images_exist = item.influencer_shop_item.images.exists()
        if images_exist:
            image = item.influencer_shop_item.images.first().image.url
        else:
            image = None
        product_sku_features = ProductFeatureValue.objects.filter(
            product_id=item.product_sku.product.pk, feature__is_deleted=False
        ).values("number_value", "string_value", "boolean_value", "feature__name")
        if (
            item.product_sku.compare_at_price
            and item.total_price == item.product_sku.price
        ):
            discounted_price = item.total_price
            item_price = item.product_sku.compare_at_price
        else:
            item_price = item.product_sku.price
            discounted_price = None
        good_info = get_product_info(
            product_sku_features,
            image,
            item.influencer_shop_item.name,
            item_price,
            discounted_price,
        )
        goods_info.append(good_info)
    html = _render(
        "emails/redesigned_emails/successfull-order.html",
        {
            "user": user,
            "goods_info": goods_info,
            "order_number": order_merchant.pk,
            "link": settings.BUYER_ORDER_LINK.format(order_merchant.pk),
        },
    )
    if only_render:
        return html
    send_html_email(
        _("Thank you for your purchase"),
        [user.email],
        html,
    )


@app.task(queue="emails")
def send_package_status_changed_email(
    user_id: int, merchant_order_id: int, only_render=False
):
    user = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    goods_info = []
    merchant_order = MerchantOrder.objects.filter(id=merchant_order_id).first()
    buyer_order = merchant_order.buyer_order
    delivery_date = merchant_order.deliver_date
    items_in_order = ProductSKUMerchantOrder.objects.filter(
        merchant_order_id=merchant_order_id
    ).prefetch_related("influencer_shop_item__images", "product_sku__product")
    subject = None
    for item in items_in_order:
        images_exist = item.influencer_shop_item.images.exists()
        if images_exist:
            image = item.influencer_shop_item.images.first().image.url
        else:
            image = None
        product_sku_features = ProductFeatureValue.objects.filter(
            product_id=item.product_sku.product.pk, feature__is_deleted=False
        ).values("number_value", "string_value", "boolean_value", "feature__name")
        if (
            item.product_sku.compare_at_price
            and item.total_price == item.product_sku.price
        ):
            discounted_price = item.total_price
            item_price = item.product_sku.compare_at_price
        else:
            item_price = item.product_sku.price
            discounted_price = None
        good_info = get_product_info(
            product_sku_features,
            image,
            item.influencer_shop_item.name,
            item_price,
            discounted_price,
        )
        goods_info.append(good_info)

    if buyer_order.status == BuyerOrderStatus.received:
        subject = _("Your order is delivered!")
    elif buyer_order.status == BuyerOrderStatus.shipped:
        subject = _("Your order is on the way!")
    elif buyer_order.status == BuyerOrderStatus.returned:
        subject = _("Product return confirmation")
    elif buyer_order.status == BuyerOrderStatus.ready_to_pick_up:
        subject = _("Your package has arrived!")
    html = _render(
        "emails/redesigned_emails/package-status-changed.html",
        {
            "user": user,
            "goods_info": goods_info,
            "order_number": merchant_order.pk,
            "link": settings.BUYER_ORDER_LINK.format(merchant_order_id),
            "delivery_date": delivery_date,
            "status": buyer_order.status,
        },
    )
    if only_render:
        return html
    send_html_email(
        subject,
        [user.email],
        html,
    )


@app.task(queue="emails")
def send_order_cancelled_email(user_id: int, order_id: int, only_render=False):
    user = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    order = api.models.order.buyer_order.BuyerOrder.objects.filter(id=order_id).first()
    order_merchant = MerchantOrder.objects.filter(buyer_order=order).first()
    goods_info = []
    items_in_order = ProductSKUMerchantOrder.objects.filter(
        merchant_order_id=order_merchant.pk
    ).prefetch_related("influencer_shop_item__images", "product_sku__product")
    for item in items_in_order:
        images_exist = item.influencer_shop_item.images.exists()
        if images_exist:
            image = item.influencer_shop_item.images.first().image.url
        else:
            image = None
        product_sku_features = ProductFeatureValue.objects.filter(
            product_id=item.product_sku.product.pk, feature__is_deleted=False
        ).values("number_value", "string_value", "boolean_value", "feature__name")
        if (
            item.product_sku.compare_at_price
            and item.total_price == item.product_sku.price
        ):
            discounted_price = item.total_price
            item_price = item.product_sku.compare_at_price
        else:
            item_price = item.product_sku.price
            discounted_price = None
        good_info = get_product_info(
            product_sku_features,
            image,
            item.influencer_shop_item.name,
            item_price,
            discounted_price,
            settings.BUYER_ORDER_LINK.format(order_merchant.pk),
        )
        goods_info.append(good_info)
    html = _render(
        "emails/redesigned_emails/parcel-canceled.html",
        {
            "user": user,
            "goods_info": goods_info,
            "order_number": order_merchant.pk,
            "link": settings.BUYER_ORDER_LINK.format(order_merchant.pk),
        },
    )
    if only_render:
        return html
    send_html_email(
        _("Your order is canceled:("),
        [user.email],
        html,
    )


@app.task(queue="emails")
def send_new_message_email(user_id: int, only_render=False):
    user = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    html = _render(
        "emails/redesigned_emails/new_message.html", {"link": settings.CHAT_LINK}
    )

    if only_render:
        return html
    send_html_email(_("New message from the support"), [user.email], html)


@app.task(queue="emails")
def send_merchant_new_order_email(
    user_id: int, merchant_order_id: int, only_render=False
):
    sleep(5)  # TODO remove after testing ordering flow
    order = MerchantOrder.objects.filter(id=merchant_order_id).first()
    merchant = User.objects.filter(id=user_id).first()
    translation.activate(settings.LANGUAGE_CODE)
    goods_info = []
    items_in_order = ProductSKUMerchantOrder.objects.filter(
        merchant_order_id=merchant_order_id
    ).prefetch_related("influencer_shop_item__images", "product_sku__product")
    for item in items_in_order:
        images_exist = item.influencer_shop_item.images.exists()
        if images_exist:
            image = item.influencer_shop_item.images.first().image.url
        else:
            image = None
        product_sku_features = ProductFeatureValue.objects.filter(
            product_id=item.product_sku.product.pk, feature__is_deleted=False
        ).values("number_value", "string_value", "boolean_value", "feature__name")
        good_info = get_product_info(
            product_sku_features,
            image,
            item.influencer_shop_item.name,
            item.product_sku.price,
        )
        goods_info.append(good_info)
    date_created_with_timezone = timezone.make_aware(
        order.created_at
        + datetime.timedelta(days=settings.MERCHANT_ORDER_APPROVAL_DAYS_ALLOCATED),
        timezone=pytz.timezone(settings.TIME_ZONE),
    )
    html = _render(
        "emails/merchant/new_order.html",
        {
            "user": merchant,
            "goods_info": goods_info,
            "order_number": merchant_order_id,
            "link": settings.MERCHANT_ORDER_LINK.format(merchant_order_id),
            "link_to_orders": settings.MERCHANT_ORDER_LINK.format(""),
            "time": f"{date_created_with_timezone.strftime('%H:%M')}, "
            f"{date_created_with_timezone.strftime('%d.%m.%Y')}",
        },
    )
    if only_render:
        return html
    send_html_email(_("New order received"), [merchant.email], html)


@app.task(queue="periodic")
def send_merchant_out_of_stock_email(only_render=False):
    merchants = User.objects.filter(is_merchant=True)
    for merchant in merchants:
        translation.activate(settings.LANGUAGE_CODE)
    for merchant in merchants:
        yesterday = datetime.datetime.utcnow().date() - datetime.timedelta(days=1)
        products_sku = ProductSKU.objects.filter(
            stock_updated_date__date=yesterday,
            product__merchant_id=merchant.pk,
            in_stock=0,
        ).prefetch_related("product")
        goods_info = []
        for product_sku in products_sku:
            images_exist = InfluencerShopItemImage.objects.filter(
                influencer_shop_item__product=product_sku.product
            ).exists()
            if images_exist:
                image = (
                    InfluencerShopItemImage.objects.filter(
                        influencer_shop_item__product=product_sku.product
                    )
                    .first()
                    .image.url
                )
            else:
                image = None
            product_sku_features = ProductFeatureValue.objects.filter(
                product_id=product_sku.product, feature__is_deleted=False
            ).values("number_value", "string_value", "boolean_value", "feature__name")
            good_info = get_product_info(
                product_sku_features, image, product_sku.product.name, product_sku.price
            )
            goods_info.append(good_info)
        html = _render(
            "emails/merchant/out_of_stock.html",
            {
                "user": merchant,
                "goods_info": goods_info[0:10],
                "amount": len(products_sku),
                "goods_to_collapse_into_link": len(products_sku) - 10,
                "link": settings.MERCHANT_PRODUCTS_PAGE,
            },
        )
        if only_render:
            return html
        send_html_email(
            _("These items are out of stock, please update the balance"),
            [merchant.email],
            html,
        )


@app.task(queue="periodic")
def send_item_in_stock_email(only_render=False):
    updated_stock_skus_ids = ProductSKU.objects.filter(
        Q(stock_updated_date__hour__gte=settings.ITEM_IN_STOCK_EMAIL_HOUR)
        & Q(stock_updated_date__lte=datetime.date.today())
    ).values_list("id", flat=True)
    notifications_to_send = ItemAvailabilitySubscription.not_expired.filter(
        product_sku_id__in=updated_stock_skus_ids
    )
    users_to_send_notification = notifications_to_send.values_list(
        "user_id", flat=True
    ).distinct("id")
    for user_id in users_to_send_notification:
        user = User.objects.filter(pk=user_id).first()
        notifications = notifications_to_send.filter(user_id=user)
        goods_info = []
        for notification in notifications:
            influencer_shop_item = (
                InfluencerShopItem.objects.filter(
                    pk=notification.influencer_shop_item.pk
                )
                .prefetch_related("images")
                .first()
            )
            image_exists = influencer_shop_item.images.exists()
            if image_exists:
                image = influencer_shop_item.images.first().image.url
            else:
                image = None
            product_sku = (
                ProductSKU.objects.filter(pk=notification.product_sku.pk)
                .prefetch_related("product")
                .first()
            )
            product = product_sku.product
            product_sku_features = ProductFeatureValue.objects.filter(
                product_id=product.pk, feature__is_deleted=False
            ).values("number_value", "string_value", "boolean_value", "feature__name")
            if notification.product_sku.compare_at_price:
                new_product_price = notification.product_sku.price
                old_price = notification.product_sku.compare_at_price
            else:
                new_product_price = None
                old_price = notification.product_sku.price
            good_info = get_product_info(
                product_sku_features,
                image,
                influencer_shop_item.name,
                old_price,
                new_product_price,
                settings.ITEM_PAGE_LINK.format(
                    influencer_shop_item.influencer_shop.external_id,
                    influencer_shop_item.collection.pk,
                    influencer_shop_item.influencer_shop.pk,
                ),
            )
            goods_info.append(good_info)
            html = _render(
                "emails/redesigned_emails/in-stock-again.html",
                {
                    "user": user,
                    "goods_info": goods_info,
                },
            )
            if only_render:
                return html
            send_html_email(
                _("%(user_name)s, look what's back in stock!")
                % {"user_name": user.first_name},
                [user.email],
                html,
            )
    notifications_to_send.delete()
    ItemAvailabilitySubscription.objects.filter(
        expires_at__lte=datetime.date.today()
    ).delete()
