logger = logging.getLogger(__name__)


def _render(template, context: dict):
    context.update(
        {
            "footer_company_name": settings.EMAIL_FOOTER_COMPANY_NAME,
            "footer_company_address": settings.EMAIL_FOOTER_COMPANY_ADDRESS,
            "support_email": settings.SUPPORT_EMAIL,
            "current_year": date.today().year,
        }
    )
    return render_to_string(template, context)


def get_email_text_variant(html_message):
    # convert links to "Link text (https://example.com)"
    regex = (
        r"<a[\sA-z=\"\-:;#0-9,_']*href=\"\s*(https?:\/\/[A-z\.\-_\/\?0-9=]+)\s*\"[\sA-z=\"\-:;#0-9,_']*>"
        r"([A-zА-я\s]+)"
        r"<\/a>"
    )
    html_message = re.sub(regex, r"\2 (\1)", html_message)

    root: _Element = etree.fromstring(
        html_message, etree.HTMLParser(remove_comments=True)
    )
    body: _Element = root.find("body")
    text = "".join(body.itertext())

    # remove extra empty lines
    lines = []
    empty_lines_count = 0
    for line in text.split("\n"):
        line = line.strip()
        if len(line) == 0:
            empty_lines_count += 1
            if empty_lines_count > 1:
                continue
        else:
            empty_lines_count = 0
        lines.append(line)
    return "\n".join(lines)


def send_html_email(subject: str, recipients: Iterable[str], html_message: str):
    try:
        if all(i.strip() == "" for i in recipients):
            raise RuntimeError("Empty recipients list")

        connection = mail.get_connection()
        from_email = settings.DEFAULT_FROM_EMAIL

        for recipient in recipients:
            if recipient != "":
                mail.send_mail(
                    subject=subject,
                    message=get_email_text_variant(str(html_message)),
                    from_email=from_email,
                    recipient_list=[recipient],
                    connection=connection,
                    html_message=html_message,
                    fail_silently=False,
                )
    except Exception:
        logger.exception("failed to send mail")
        capture_exception()
