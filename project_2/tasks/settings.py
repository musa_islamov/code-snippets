# example of celery settings for aws sqs queue


CELERY_TASK_ALWAYS_EAGER = (
    os.environ.get("CELERY_TASK_ALWAYS_EAGER", "false").lower() == "true"
)
SQS_ACCESS_KEY_ID = os.environ.get("SQS_ACCESS_KEY_ID")
SQS_SECRET_ACCESS_KEY = os.environ.get("SQS_SECRET_ACCESS_KEY")
CELERY_DYNAMODB_ENDPOINT_URL = os.environ.get("CELERY_DYNAMODB_ENDPOINT_URL")
CELERY_RESULT_BACKEND = (
    f"dynamodb://{SQS_ACCESS_KEY_ID}:{SQS_SECRET_ACCESS_KEY}@{'etnu1jktcj0i73q590hl'}/"
)
QUEUE_DEFAULT = "default"
SQS_QUEUE = os.environ.get("SQS_QUEUE")
if SQS_QUEUE:
    CELERY_BROKER_TRANSPORT_OPTIONS = {
        "is_secure": True,
        "predefined_queues": {
            "default": {
                "url": SQS_QUEUE,
                "access_key_id": os.environ.get("SQS_ACCESS_KEY_ID"),
                "secret_access_key": os.environ.get("SQS_SECRET_ACCESS_KEY"),
            },
            "periodic": {
                "url": SQS_QUEUE,
                "access_key_id": os.environ.get("SQS_ACCESS_KEY_ID"),
                "secret_access_key": os.environ.get("SQS_SECRET_ACCESS_KEY"),
            },
            "celery": {
                "url": SQS_QUEUE,
                "access_key_id": os.environ.get("SQS_ACCESS_KEY_ID"),
                "secret_access_key": os.environ.get("SQS_SECRET_ACCESS_KEY"),
            },
        },
        "region": os.environ.get("SQS_REGION"),
    }