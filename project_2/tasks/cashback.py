@app.task(queue=settings.QUEUE_DEFAULT)
def add_pending_earnings(buyer_order_id: int):
    with transaction.atomic():
        logging.debug("Started updating cashback for user with id %s", buyer_order_id)
        influencer_earnings_history_to_create = []
        merchant_order = MerchantOrder.objects.filter(
            buyer_order_id=buyer_order_id
        ).prefetch_related("product_skus_merchant_order")
        items = []
        for order in merchant_order:
            items.extend(order.product_skus_merchant_order.all())
        for item in items:
            price_of_ordered_item = item.total_price
            influencer_shop_item = item.influencer_shop_item
            influencer = influencer_shop_item.influencer_shop.influencer
            count = item.count
            product = influencer_shop_item.product
            product_category = product.category
            commission_value = get_commission_value(
                merchant_info=item.merchant_order.merchant_info,
                category=product_category,
            )
            product_category_percent_earning = (
                influencer.influencer_info.earnings_percent
                / 100
                * commission_value
                / 100
            )
            earnings_to_add = round(
                price_of_ordered_item * product_category_percent_earning * count
            )
            influencer_earnings_history_to_create.append(
                InfluencerEarningsSourcesHistory(
                    influencer=influencer,
                    order_id=buyer_order_id,
                    earning_percent=product_category_percent_earning * 100,
                    amount=earnings_to_add,
                    type=InfluencerEarningSourceType.in_percent_from_order,
                    status=InfluencerEarningSourceStatus.pending,
                    count_of_products=count,
                    product=product,
                    product_category=product.category,
                )
            )
        InfluencerEarningsSourcesHistory.objects.bulk_create(
            influencer_earnings_history_to_create
        )


@app.task(queue=settings.QUEUE_DEFAULT)
def approve_pending_earnings(buyer_order_id: int):
    with transaction.atomic():
        pending_earnings = InfluencerEarningsSourcesHistory.objects.filter(
            order_id=buyer_order_id,
            status=InfluencerEarningSourceStatus.pending,
            type=InfluencerEarningSourceType.in_percent_from_order,
        ).select_for_update()
        if pending_earnings:
            ifluencer_info_to_update = set()
            for pending_earning in pending_earnings:
                pending_earning.influencer.influencer_info.earnings += (
                    pending_earning.amount
                )
                ifluencer_info_to_update.add(pending_earning.influencer.influencer_info)
            Influencer.objects.bulk_update(list(ifluencer_info_to_update), ["earnings"])
            pending_earnings.update(
                status=InfluencerEarningSourceStatus.success,
                approved_at=datetime.utcnow(),
            )
        else:
            logging.critical("Empty pending earnings for BuyerOrder#%s", buyer_order_id)


@app.task(queue=settings.QUEUE_DEFAULT)
def cancel_pending_earnings(buyer_order_id: int):
    with transaction.atomic():
        pending_earnings = InfluencerEarningsSourcesHistory.objects.filter(
            order_id=buyer_order_id,
            status=InfluencerEarningSourceStatus.pending,
            type=InfluencerEarningSourceType.in_percent_from_order,
        ).select_for_update()
        if pending_earnings:
            pending_earnings.update(status=InfluencerEarningSourceStatus.cancelled)
        else:
            logging.critical("Empty pending earnings for BuyerOrder#%s", buyer_order_id)
