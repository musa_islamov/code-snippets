class Public_InfluencerShopBestsellersViewSet(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
):
    serializer_class = Public_InfluencerShopItemListSerializer
    permission_classes = [AllowAny]
    pagination_class = StrictLimitOffsetPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_class = PublicBestsellersFilterSet

    def filter_queryset(self, queryset):
        if self.action in ("list",):
            return super().filter_queryset(queryset)
        return queryset

    def get_queryset(self):
        """Doing so because django orm doesn't have annotate+distinct implementation"""
        influencer_shop_items = (
            InfluencerShopItem.not_deleted.filter(
                product__is_deleted=False,
                product__status=ProductStatus.published,
                is_published=True,
            )
            .annotate(num_sells=Count("product_sku_merchant_orders"))
            .order_by("-num_sells", "skus__price")
        )
        values = OrderedDict(influencer_shop_items.values_list("product_id", "id"))
        ordered_ids_to_filter = builtins.list(values.values())
        preserved = Case(
            *[When(pk=pk, then=pos) for pos, pk in enumerate(ordered_ids_to_filter)]
        )
        queryset = (
            InfluencerShopItem.objects.filter(pk__in=ordered_ids_to_filter)
            .prefetch_related("product_sku_merchant_orders__product_sku")
            .prefetch_related("images")
            .prefetch_sku_with_min_price()
            .prefetch_popular_sku()
            .order_by(preserved)
        )
        return queryset

    @swagger_auto_schema(manual_parameters=PublicBestsellersFilterSet.parameters)
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
