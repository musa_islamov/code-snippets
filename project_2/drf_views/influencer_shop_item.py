class Public_InfluencerShopItemViewSet(
    viewsets.GenericViewSet,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
):
    """This Viewset is needed for any user to work with Influencer shop items"""

    permission_classes = [AllowAny]
    pagination_class = StrictLimitOffsetPagination

    def get_serializer_class(self):
        return Public_InfluencerShopItemReadSerializer

    def get_queryset(self):
        return (
            InfluencerShopItem.not_deleted.filter(
                product__is_deleted=False,
                product__status=ProductStatus.published,
                is_published=True,
            )
            .select_related(
                "collection",
                "influencer_shop__influencer__influencer_info",
            )
            .prefetch_related(
                Prefetch(
                    "images",
                    queryset=InfluencerShopItemImage.ordered_by_index.all(),
                )
            )
            .prefetch_related(
                "product__features__feature__select_options",
                "product__features__select_option",
                "product__parameters__choices__parameter",
            )
            .prefetch_related(
                Prefetch(
                    "skus",
                    queryset=ProductSKU.not_deleted.prefetch_related(
                        "parameter_values__parameter",
                    ),
                )
            )
        )

    class _ProductIdsSerializer(serializers.Serializer):
        product_ids = serializers.ListField(child=serializers.IntegerField(min_value=1))

    _viewed_response_serializer = Public_InfluencerShopItemReadSerializer(many=True)

    @swagger_auto_schema(
        request_body=_ProductIdsSerializer, responses={200: _viewed_response_serializer}
    )
    @action(methods=["POST"], detail=False)
    def viewed(self, request: Request, *args, **kwargs) -> Response:
        serializer = self._ProductIdsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = self.get_queryset().filter(
            product_id__in=serializer.validated_data["product_ids"]
        )
        response_serializer = self.get_serializer_class()(instance=data, many=True)
        return Response(data=response_serializer.data)


class Public_InfluencerShopItemByProductIdViewSet(
    viewsets.GenericViewSet,
    mixins.RetrieveModelMixin,
):
    """This Viewset is needed for any user to retrieve shop items by product.id"""

    permission_classes = [AllowAny]
    lookup_field = "product_id"

    def get_serializer_class(self):
        return Public_InfluencerShopItemReadSerializer

    def get_queryset(self):
        return (
            InfluencerShopItem.not_deleted.filter(is_published=True, is_deleted=False)
            .select_related(
                "product",
                "product__category",
                "collection",
                "influencer_shop",
                "influencer_shop__influencer",
                "influencer_shop__influencer__influencer_info",
            )
            .prefetch_related(
                "product__features",
                "product__parameters",
                "product__reviews",
                "skus",
                "skus__parameter_values",
                "images",
            )
        )
