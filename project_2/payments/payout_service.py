class PayoutProvider:
    def pay(
        self,
        merchant_info: Merchant,
        amount_to_pay_to_merchant: int,
        merchant_earnings_sources_history: MerchantEarningsSourcesHistory,
    ):
        pass


class StripeMerchantPayoutsProvider(PayoutProvider):
    @classmethod
    def pay(
        self,
        merchant_info: Merchant,
        amount_to_pay_to_merchant: int,
        merchant_earnings_sources_history: MerchantEarningsSourcesHistory,
    ):
        try:
            if (
                merchant_info.stripe_user_id is not None
                and merchant_info.stripe_user_id
            ):
                transfer = stripe.Transfer.create(
                    amount=amount_to_pay_to_merchant,
                    currency="usd",
                    destination=merchant_info.stripe_user_id,
                ).get("id")
                merchant_earnings_sources_history.transfer_id = transfer
                merchant_earnings_sources_history.status = (
                    MerchantEarningSourceStatus.success
                )
                merchant_earnings_sources_history.save()
        except StripeError:
            raise BadRequest(_("Cannot accrue earnings to merchant"))


@app.task(queue=settings.QUEUE_DEFAULT)
def payout(buyer_order_id, payment_type):
    buyer_order = BuyerOrder.objects.filter(pk=buyer_order_id).first()
    product_sku_merchant_orders = ProductSKUMerchantOrder.objects.filter(
        merchant_order__buyer_order=buyer_order
    ).values("merchant_order", "product_sku")
    product_sku_merchant_orders_for_influencer_orders = (
        ProductSKUMerchantOrderForInfluencerOrder.objects.filter(
            merchant_order__buyer_order=buyer_order
        ).values("merchant_order", "product_sku")
    )
    if (
        product_sku_merchant_orders is None
        or product_sku_merchant_orders_for_influencer_orders is None
    ):
        raise well_formatted_validation_error(
            _("Such ProductSKUMerchantOrder doesn't exist")
        )
    merchant_orders_and_product_skus = list(
        chain(
            product_sku_merchant_orders,
            product_sku_merchant_orders_for_influencer_orders,
        )
    )
    for item in merchant_orders_and_product_skus:
        merchant_order = (
            MerchantOrder.objects.filter(pk=item.get("merchant_order"))
            .prefetch_related("merchant_info__merchant_user")
            .first()
        )
        product_sku = (
            ProductSKU.objects.filter(pk=item.get("product_sku"))
            .prefetch_related("product")
            .first()
        )
        merchant_info = merchant_order.merchant_info
        product = product_sku.product
        merchant_commission = Commission.objects.filter(
            merchant_info=merchant_info,
            category=product.category,
        ).first()
        if not merchant_commission:
            merchant_commission = settings.DEFAULT_COMMISSION
        amount_to_pay_to_merchant = int(
            (buyer_order.total_price - buyer_order.delivery_price)
            * merchant_commission
            / 100
        )
        merchant_earnings_sources_history = (
            MerchantEarningsSourcesHistory.objects.create(
                merchant_info=merchant_info,
                order=buyer_order,
                earning_percent=merchant_commission,
                amount=amount_to_pay_to_merchant,
                product_category=product.category,
                product=product,
                status=MerchantEarningSourceStatus.pending,
            )
        )
        merchant_info.total_earnings += amount_to_pay_to_merchant
        merchant_info.save()
        globals()[payment_type].pay(
            merchant_info, amount_to_pay_to_merchant, merchant_earnings_sources_history
        )


PAYMENT_PAYOUTS_PROVIDER_MAPPING = {PaymentType.stripe: StripeMerchantPayoutsProvider}


class MerchantPayoutsManager:
    def __init__(self, payment_type: PaymentType, buyer_order: BuyerOrder):
        self.payment_type = payment_type
        self.buyer_order = buyer_order
        self.merchant_payouts_provider = PAYMENT_PAYOUTS_PROVIDER_MAPPING[
            PaymentType.stripe
        ]

    def pay_to_merchant(self):
        payout.delay(self.buyer_order.pk, self.merchant_payouts_provider.__name__)
