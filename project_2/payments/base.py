class PaymentSystem(ABC):
    payment_system_type = None

    def freeze_money(
        self, amount: int, user: User, additional_data=None, *args, **kwargs
    ) -> dict or False:
        """
        This method freezes money on buyer bank account, so if merchant would have some problems
        It can easily returned
        """

        pass

    def collect_frozen_money(self, *args, **kwargs):
        """
        When merchant approves order and delivery, system needs to make transaction with to pay for order via
        previously frozen money
        """
        pass

    def process(
        self,
        buyer_order: BuyerOrder,
        amount_to_pay: int,
        additional_payment_data: Optional[dict] = None,
        *args,
        **kwargs,
    ):
