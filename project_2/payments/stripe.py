class StripePaymentSystem(PaymentSystem):
    payment_system_type = PaymentType.stripe

    def __init__(self):
        stripe.api_version = settings.STRIPE_API_VERSION

    def set_api_keys(self, country):
        if country.iso_code == "US":
            stripe.api_key = settings.STRIPE_KEY
        if country.iso_code == "BR":
            stripe.api_key = settings.STRIPE_KEY_BRAZIL

    def create_order(
        self,
        currency: str,
        price: float,
        customer: str,
        save_card: bool,
        delivery_address: dict,
        user: User,
        payment_method_id: str,
    ):
        if save_card and check_if_card_not_added(payment_method_id, customer):
            setup_future_usage = "on_session"
        else:
            setup_future_usage = None
        product_id = stripe.Product.create(
            name=f"order line item from {customer}, ordered at: {datetime.now()}",
            default_price_data={
                "currency": currency,
                "unit_amount_decimal": price,
                "tax_behavior": "exclusive",
            },
            id=f"{customer}_{datetime.now().strftime('%m_%d_%Y__%H_%M_%S')}",
        ).get("id")
        order = stripe.Order.create(
            currency=currency,
            line_items=[{"product": product_id}],
            customer=customer,
            payment={
                "settings": {
                    "payment_method_types": ["card"],
                    "payment_method_options": {
                        "card": {
                            "capture_method": "manual",
                            "setup_future_usage": setup_future_usage,
                        },
                    },
                },
            },
            automatic_tax={"enabled": False},
            shipping_details={
                "address": delivery_address,
                "name": f"{user.first_name} {user.last_name}",
            },
        )
        return order

    def create_payment_intent(
        self,
        customer: str,
        payment_method_id: str,
        currency: str,
        amount,
        save_card: bool,
    ):
        if save_card:
            return stripe.PaymentIntent.create(
                customer=customer,
                setup_future_usage="on_session",
                payment_method=payment_method_id,
                currency=currency,
                amount=amount,
                capture_method="manual",
                confirmation_method="automatic",
            )
        else:
            return stripe.PaymentIntent.create(
                customer=customer,
                payment_method=payment_method_id,
                currency=currency,
                amount=amount,
                capture_method="manual",
                confirmation_method="automatic",
            )

    def update_payment_intent(
        self,
        payment_method_id: str,
        payment_intent_id,
    ):
        stripe.PaymentIntent.modify(
            payment_intent_id,
            payment_method=payment_method_id,
        )

    def get_or_create_stripe_customer(self, user: User, country: Country):
        payment_provider = user.payment_provider_accounts.filter(
            country=country
        ).first()
        if payment_provider is not None:
            return payment_provider.payment_provider_account
        else:
            customer = stripe.Customer.create(email=user.email)
            UserPaymentProviderAccount.objects.create(
                user=user, country=country, payment_provider_account=customer.id
            )
            return customer.id

    def freeze_money(
        self, amount: int, user: User, additional_data=None, *args, **kwargs
    ) -> dict or False:
        if not isinstance(additional_data, dict):
            raise RuntimeError(
                "Additional_data should be dict and contain payment method"
            )
        if "payment_method_id" not in additional_data.keys():
            raise RuntimeError("Additional_data should contain payment method")
        if "country_id" not in additional_data.keys():
            raise RuntimeError("Additional_data should contain country")
        country = Country.objects.filter(id=additional_data["country_id"]).first()
        if not country:
            raise RuntimeError("No such country exists")
        try:
            customer = self.get_or_create_stripe_customer(user, country)
            delivery_address = get_field_or_exception(
                additional_data, "delivery_address"
            )
            order = self.create_order(
                currency=additional_data["currency"],
                price=amount,
                customer=customer,
                save_card=additional_data["save_card"],
                delivery_address=delivery_address,
                user=user,
                payment_method_id=additional_data["payment_method_id"],
            )
            order_id = order.get("id")
            total_price = order.get("amount_total")
            tax_amount = order.get("total_details").get("amount_tax")
            confirmed_order = stripe.stripe_object.StripeObject().request(
                "post", f"/v1/orders/{order_id}/submit", {"expected_total": total_price}
            )
            payment_intent_id = confirmed_order.get("payment").get("payment_intent")
            self.update_payment_intent(
                payment_intent_id=payment_intent_id,
                payment_method_id=additional_data["payment_method_id"],
            )
            payment_data = {
                "TransactionId": payment_intent_id,
                "currency": additional_data["currency"],
                "tax_amount": tax_amount,
            }
            return payment_data
        except stripe.error.InvalidRequestError:
            return False
        except stripe.error.CardError:
            return False
        except StripeError:
            return False

    def collect_frozen_money(self, data: dict, *args, **kwargs) -> bool:
        required_fields = ["transaction_id"]
        is_data_contain_required_fields = len(
            set(required_fields).intersection(set(data.keys()))
        ) == len(required_fields)
        if not is_data_contain_required_fields:
            raise RuntimeError(
                f"Data to confirm transaction secure should contain {required_fields}, "
                f"but has {data.keys()}"
            )
        try:
            stripe.PaymentIntent.confirm(data["transaction_id"])
            stripe.PaymentIntent.capture(data["transaction_id"])
            return True
        except StripeError:
            return False

    def extract_bank_card_data(self, user: User, additional_data: dict):
        try:
            to_save_card = additional_data.get("to_save_card", False)
            payment_method_id = get_field_or_exception(
                additional_data, "payment_method_id"
            )
            bank_card_data = retrieve_bank_card_data(payment_method_id)
            bank_card, created = get_bank_card_obj(
                self, to_save_card, bank_card_data, user
            )
        except StripeBankCardRetrievalError:
            bank_card, created = None, False

        return bank_card, created

    def process(
        self,
        buyer_order: BuyerOrder,
        amount_to_pay: int,
        additional_payment_data: Optional[dict] = None,
        *args,
        **kwargs,
    ):
        with transaction.atomic():
            if "country_id" not in additional_payment_data.keys():
                raise RuntimeError("Additional_data should contain country")
            country = Country.objects.filter(
                id=additional_payment_data["country_id"]
            ).first()
            if not country:
                raise RuntimeError("No such country exists")
            self.set_api_keys(country)
            bank_card, created = self.extract_bank_card_data(
                buyer_order.buyer,
                additional_payment_data,
            )
            payment_data: dict = self.freeze_money(
                amount=amount_to_pay,
                user=buyer_order.buyer,
                additional_data=additional_payment_data,
            )
            if payment_data is False:
                raise well_formatted_validation_error("Error in making a payment")
            tax_amount = payment_data.get("tax_amount")
            if tax_amount is None:
                tax_amount = 0
            buyer_order.total_price += tax_amount
            amount_to_pay += tax_amount
            buyer_order.save()
            transaction_id = payment_data.get("TransactionId")
            payment = Payment.objects.create(
                additional_data=payment_data,
                transaction_id=transaction_id,
                price=amount_to_pay,
                type=self.payment_system_type,
                bank_card=bank_card,
            )
            confirm = self.collect_frozen_money(
                {"transaction_id": payment.transaction_id}
            )
            buyer_order.payment = payment
            if confirm:
                merchant_payouts_manager = MerchantPayoutsManager(
                    payment_type=PaymentType.stripe,
                    buyer_order=buyer_order,
                )
                merchant_payouts_manager.pay_to_merchant()
                payment.status = PaymentStatus.succeed
                buyer_order.status = BuyerOrderStatus.processing
                buyer_order.save()
            if not confirm:
                buyer_order.status = BuyerOrderStatus.waiting_payment
                buyer_order.save()
                raise well_formatted_validation_error(
                    _("An error has occurred. Please, try again later")
                )
            payment.save()
            send_successful_order_email.delay(
                buyer_order.buyer.pk,
                buyer_order.pk,
            )
            if created:
                bank_card.verify_external_id = payment.transaction_id
                bank_card.save()
            return buyer_order
