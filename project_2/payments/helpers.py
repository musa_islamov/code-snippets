def get_additional_payment_data(
    request: Request,
    serializer: serializers.ModelSerializer,
    payment_system: PaymentSystem,
) -> dict:
    pay_with_earnings = serializer.validated_data.get("pay_with_earnings", False)
    if payment_system.payment_system_type == PaymentType.stripe:
        data = serializer.validated_data
        cart_item_and_delivery_addresses = get_field_or_exception(
            data, "cart_and_delivery_addresses"
        )
        delivery_address = get_field_or_exception(
            cart_item_and_delivery_addresses[0], "delivery_address"
        )
        if (
            cart_item_and_delivery_addresses[0].get("cart_item").__class__.__name__
            == "CartItem"
        ):
            country_id = (
                cart_item_and_delivery_addresses[0]
                .get("cart_item")
                .influencer_shop_item.influencer_shop.country.pk
            )
        elif (
            cart_item_and_delivery_addresses[0].get("cart_item").__class__.__name__
            == "InfluencerCartItem"
        ):
            country_id = (
                cart_item_and_delivery_addresses[0]
                .get("cart_item")
                .product_sku.product.merchant_info.home_area.country.pk
            )
        payment_method_id = get_field_or_exception(data, "payment_method_id")
        currency = get_field_or_exception(data, "currency")
        if "to_save_card" in data.keys() and data.get("to_save_card"):
            save_card = True
        else:
            save_card = False
        return {
            "payment_method_id": payment_method_id,
            "currency": currency,
            "save_card": save_card,
            "cart_item_and_delivery_address": cart_item_and_delivery_addresses,
            "delivery_address": format_delivery_address_data_for_stripe(
                delivery_address
            ),
            "pay_with_earnings": pay_with_earnings,
            "country_id": country_id,
        }


def get_payment_system_class(
    request: Request, payment_system: str
) -> Type[PaymentSystem]:
    if payment_system.lower() == "stripe":
        return StripePaymentSystem
    raise RuntimeError()


def get_or_create_stripe_customer(user: User, country_id: int):
    payment_provider = user.payment_provider_accounts.filter(
        country_id=country_id
    ).first()
    if payment_provider is not None:
        return payment_provider.payment_provider_account
    else:
        customer = stripe.Customer.create(email=user.email)
        UserPaymentProviderAccount.objects.create(
            user=user, country_id=country_id, payment_provider_account=customer.id
        )
        return customer.id


def format_delivery_address_data_for_stripe(delivery_address: DeliveryAddress):
    city = delivery_address.locality.name
    country = delivery_address.country.iso_code
    line_1 = f"{delivery_address.street}"
    line_2 = f"{delivery_address.building}"
    return {"city": city, "country": country, "line1": line_1, "line2": line_2}
