@transaction.atomic
def pay(
    payment_system: PaymentSystem,
    bank_card: BankCard,
    user: User,
    total_price: int,
    additional_payment_data: dict,
    buyer_order: BuyerOrder = None,
) -> Payment:
    with transaction.atomic():
        payment_data: dict = payment_system.freeze_money(
            amount=total_price,
            user=user,
            additional_data=additional_payment_data,
        )
        if payment_data is False:
            raise well_formatted_validation_error("Error in making a payment")
        tax_amount = payment_data.get("tax_amount")
        if tax_amount is None:
            tax_amount = 0
        buyer_order.total_price += tax_amount
        buyer_order.save()
        transaction_id = payment_data.get("TransactionId")

        return Payment.objects.create(
            additional_data=payment_data,
            transaction_id=transaction_id,
            price=total_price,
            type=payment_system.payment_system_type,
            bank_card=bank_card,
        )
