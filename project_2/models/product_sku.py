class ProductSKU(BaseModel):
    product = models.ForeignKey(
        "api.Product",
        on_delete=models.PROTECT,
        related_name="skus",
    )

    external_id = models.BigIntegerField(null=True, blank=True)
    external_source = models.CharField(
        max_length=32,
        choices=Integration.choices,
        null=True,
        blank=True,
    )
    price = models.PositiveIntegerField(verbose_name=_("Current price"))
    compare_at_price = models.PositiveIntegerField(
        null=True, blank=True, verbose_name=_("Price before applying discount")
    )
    in_stock = models.PositiveIntegerField()
    in_stock_previously = models.PositiveIntegerField(null=True, blank=True)
    parameter_values = models.ManyToManyField(
        "api.ProductParameterSelectOption",
        related_name="skus",
        through="api.ProductSKUParameterValue",
        help_text=_("Values of unique sku parameters"),
    )

    is_deleted = models.BooleanField(default=False)
    stock_updated_date = models.DateTimeField(
        _("Last stock update time"), auto_now_add=True, null=True
    )
    vendor_code = models.CharField(max_length=30, null=True, blank=True)
    barcode = models.CharField(max_length=128, null=True, blank=True)
    honest_sign_marking = models.CharField(max_length=128, null=True, blank=True)
    length = models.PositiveIntegerField(default=0)
    width = models.PositiveIntegerField(default=0)
    height = models.PositiveIntegerField(default=0)
    weight = models.PositiveIntegerField(default=0)
    objects = models.Manager()
    not_deleted = NotDeletedManager()

    class Meta:
        constraints = [
            UniqueConstraint(
                name="unique__external_id__external_source__constraint_for_product_sku",
                fields=["external_id", "external_source"],
                condition=Q(is_deleted=False),
            ),
        ]

    @property
    def discount_percent(self):
        if self.compare_at_price:
            return round(
                (self.compare_at_price - self.price) / self.compare_at_price * 100
            )
        else:
            return 0

    @property
    def current_price(self):
        """
        Return current price, converted from cents.
        """
        return self.price / 100

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    def save(self, *args, **kwargs):
        if self.pk is not None:
            previous_state = ProductSKU.objects.get(pk=self.pk)
            if self.in_stock != previous_state.in_stock:
                self.in_stock_previously = previous_state.in_stock
                self.stock_updated_date = self.updated_at
        super().save(*args, **kwargs)

    def validate(self, *args, **kwargs):
        # TODO: refactor
        if not self.id:
            # Это нужно чтобы проверки не выкидывали ошибки при создании
            # После создания нужно добавить зависимости
            # parameter_values один из product.parameter.choices.all()
            return

        from api.models import ProductParameter

        all_product_parameters = ProductParameter.objects.filter(product=self.product)
        all_values = self.parameter_values.all().prefetch_related("parameter")
        # Проверка на то что в sku есть кажодый параметр из product
        parameter_ids_with_values = []
        for value in all_values:
            if value.parameter not in all_product_parameters:
                raise ValidationError(
                    _(
                        "SKU must have all values from product parameters, parameter select option #%(vid)s not "
                        "found in product.parameters" % {"vid": value.id}
                    )
                )
            parameter_ids_with_values.append(value.parameter.id)
        for product_parameter in all_product_parameters:
            if product_parameter.id not in parameter_ids_with_values:
                raise ValidationError(
                    _("SKY has no value for the parameter #%(pid)s")
                    % {"pid": product_parameter.id}
                )

        # Проверка что у каждого parameter_values есть только одно значение для parameter
        has_option_for_parameter = []
        for value in all_values:
            if value.parameter.id in has_option_for_parameter:
                raise ValidationError(
                    _(
                        "SKU should contain only one parameter_value for parameter #%(pid)s, but got more"
                        % {"pid": value.parameter.id}
                    )
                )
            has_option_for_parameter.append(value.parameter.id)
        if self.compare_at_price and self.compare_at_price < self.price:
            raise ValidationError(
                _("Price before discount can't be less than price after discount")
            )

    def __str__(self):
        return (
            f"Product SKU #{self.id} {self.product.name} (product_id={self.product_id})"
