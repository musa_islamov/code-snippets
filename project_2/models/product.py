class Product(BaseModel):
    # Has related name "reviews" to ItemReview
    # Has related name "skus" to ProductSKU

    # Has related name "images"
    merchant_info = models.ForeignKey(
        "api.Merchant",
        on_delete=models.PROTECT,
        related_name="products",
    )
    external_id = models.BigIntegerField(null=True, blank=True)
    external_source = models.CharField(
        max_length=32,
        choices=Integration.choices,
        null=True,
        blank=True,
    )

    # has related name "influencer_shop_items"
    # has related name "features" from ProductFeatureValue
    # hos related name "parameters" from api.ProductParameter
    name = models.CharField(max_length=256)
    description = models.TextField(max_length=2000)

    category = models.ForeignKey(
        "api.ProductCategory",
        on_delete=models.PROTECT,
        related_name="products",
        null=True,
    )

    status = models.CharField(
        max_length=32,
        choices=ProductStatus.choices,
        default=ProductStatus.on_verification,
    )

    vendor_code = models.CharField(max_length=30, null=True, blank=True)

    barcode = models.CharField(max_length=128, null=True, blank=True)

    # error field set only if status is error
    error = models.CharField(max_length=128, null=True, blank=True)

    is_deleted = models.BooleanField(default=False)

    objects = models.Manager()
    not_deleted = NotDeletedManager()

    class Meta:
        constraints = [
            UniqueConstraint(
                name="unique__external_id__external_source__constraint_for_product",
                fields=["external_id", "external_source"],
            ),
        ]

    def get_existing_parameters(self, sku_ids: List[int] = None) -> QuerySet:
        """Return QuerySet with ProductParameter which present in not deleted SKUs"""
        from api.models import (
            ProductParameter,
            ProductParameterSelectOption,
            ProductSKUParameterValue,
        )

        if sku_ids is None:
            sku_ids = [i.id for i in self.skus.all()]

        with transaction.atomic():
            pvs = (
                ProductSKUParameterValue.objects.filter(
                    product_sku_id__in=sku_ids,
                    product_sku__is_deleted=False,
                    product_parameter_select_option__is_deleted=False,
                )
                .select_related("product_parameter_select_option")
                .values(
                    "product_parameter_select_option_id",
                    "product_parameter_select_option__parameter_id",
                )
            )

            return ProductParameter.objects.filter(
                product=self,
                id__in=[
                    i["product_parameter_select_option__parameter_id"] for i in pvs
                ],
            ).prefetch_related(
                Prefetch(
                    "choices",
                    queryset=ProductParameterSelectOption.objects.filter(
                        id__in=[i["product_parameter_select_option_id"] for i in pvs]
                    ),
                )
            )

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return f"Product #{self.id} {self.name}"

    def validate(self):
        if self.status != ProductStatus.error and self.error:
            raise ValidationError(
                _("The error field should be empty if the status is not equal to error")
            )
        elif self.status == ProductStatus.error and not self.error:
            raise ValidationError(
                _("The error field should not be empty if the status is error")
            )

        if not self.merchant_info or not self.merchant_info.merchant_user.is_merchant:
            raise ValidationError(
                _(
                    "Only users with the is_merchant flag can be the owners of the product"
                )
            )

    def save(self, *args, **kwargs):
        self.validate()
        super().save(*args, **kwargs)
